.PHONY: browse build buildprof run test coverage cleancoverage doc servers clean video play scale cpuprof memprof killpprof trace

SHELL := /bin/bash

program_binary := ./out/biosim
program_options := 
program := $(program_binary) $(program_options)
program_prof := ./out/biosim_profiling

covDir := coverage
covOut := $(covDir)/coverage.out
covHtml := $(covDir)/index.html

browser := "${BROWSER}"

host := localhost
coverServerPort := 9000
godocServerPort := 9001
pprofServerPort := 9002
traceServerPort := 9003
coverServer := $(host):$(coverServerPort)
godocServer := $(host):$(godocServerPort)
pprofServer := $(host):$(pprofServerPort)
traceServer := $(host):$(traceServerPort)

pprof_pid:=$(shell pgrep pprof)

ffmpeg := ffmpeg -loglevel error -y

browse: servers
	$(browser) $(coverServer) $(godocServer) $(pprofServer) $(traceServer)

build:
	GOOS=linux GOARCH=amd64 go build -o $(program_binary)
	GOOS=linux GOARCH=amd64 go build -o $(program_prof) -tags profiling
	GOOS=windows GOARCH=amd64 go build -o out/biosim.exe

run: clean build
	$(program_binary)

test:
	go test -coverprofile $(covOut) ./...

coverage: cleancoverage test
	@echo
	@echo "don't forget to start a godirserver before using the coverage target"
	@echo "you can run open all urls necessary (and start servers) by running 'make browse'"
	go tool cover -html $(covOut) -o $(covHtml)

cleancoverage:
	@rm -f $(covOut) $(covHtml)

doc:
	@echo "don't forget to start a godoc server before using the doc target"
	@$(browser) $(godocServer)

servers:
	godoc >/dev/null 2>/dev/null &
	godirserver >/dev/null 2>/dev/null &

clean:
	rm -f out/*

video: scale
	powershell.exe $(ffmpeg) -framerate 30 -i out/square_dumb_gen_1_round_%04d_s.png -c:v h264 -r 30 -pix_fmt yuv420p out/biosim.mp4

play:
	"/mnt/c/Program Files/VideoLAN/VLC/vlc.exe" out/biosim.mp4

scale:
	./scripts/scale.sh

cpuprof: clean build killpprof
	$(program_prof) -cpuprofile out/cpu.prof
	go tool pprof -http $(pprofServer) $(program_prof) out/cpu.prof >/dev/null 2>/dev/null &

memprof: clean build killpprof
	$(program_prof) -memprofile out/mem.prof
	go tool pprof -http $(pprofServer) $(program_prof) out/mem.prof >/dev/null 2>/dev/null &

killpprof:
ifneq ($(pprof_pid),)
	@kill -s 9 $(pprof_pid)
endif

trace: clean build
	$(program_prof) -tracefile out/trace.out
	go tool trace -http=$(traceServer) out/trace.out
