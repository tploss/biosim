package sim

import (
	"fmt"
	"image"
)

const (
	// directions
	North     uint8 = 0
	NorthEast uint8 = iota
	East
	SouthEast
	South
	SouthWest
	West
	NorthWest
)

var Directions [8]uint8 = [8]uint8{0, 1, 2, 3, 4, 5, 6, 7}

type World interface {
	// Clone will return a perfect copy of the world
	Clone() World
	// Settle will mark a random, free Coord of the world as blocked and return it
	// Use this to populate the world with creatures
	Settle() Coord
	// Place takes note of a Coord being blocked
	// Return value shows whether placement was successful
	Place(Coord) bool
	// Move should move the given Coord in the given direction
	// If there is an issue the Coord will be left unchanged
	Move(*Coord, uint8)
	// Free returns whether given Coord is free. If not it is
	// up to the caller to determine what is blocking the Coord
	Free(Coord) bool
	// Walk takes a WalkFunc and runs it on each Coord of the world
	// Walk is stopped when WalkFunc returns true or all Coords visited
	// Returns true if all Coords visited
	Walk(WalkFunc) bool
	// Rect gives the caller an image.Rect that can fit the entire world
	Rect() image.Rectangle
}

type WalkFunc func(Coord) bool

// x defines west-east; y defines north-south
// Coord 0,0  is furthest northEast
type Coord struct {
	X, Y uint8
}

func (c Coord) String() string {
	return fmt.Sprintf("(%d,%d)", c.X, c.Y)
}
