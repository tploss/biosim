package sim

import "image"

type VizComp func(World, Population) image.Image
