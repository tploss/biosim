package sim

type Creature interface {
	// Clone will return a perfect copy of the creature
	Clone() Creature
	// Act lets the creature live one iteration of a generation
	// Should return the final location of the creature
	Act(World) Coord
}

type Population map[Coord]Creature

func (pop Population) Clone() Population {
	pop2 := make(map[Coord]Creature)
	for coord, creature := range pop {
		pop2[coord] = creature.Clone()
	}
	return pop2
}

func (pop Population) Act(w World) {
	for coord, creature := range pop {
		pos := creature.Act(w)
		if pos != coord {
			delete(pop, coord)
			pop[pos] = creature
		}
	}
}
