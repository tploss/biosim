package sim_test

import (
	"fmt"
	"testing"

	"gitlab.com/tploss/biosim/sim"
)

func TestCoord_String(t *testing.T) {
	tests := []struct {
		x    uint8
		y    uint8
		want string
	}{
		{0, 0, "(0,0)"},
		{100, 100, "(100,100)"},
	}
	for _, tt := range tests {
		name := fmt.Sprintf("x: %d, y: %d", tt.x, tt.y)
		t.Run(name, func(t *testing.T) {
			c := sim.Coord{
				X: tt.x,
				Y: tt.y,
			}
			if got := c.String(); got != tt.want {
				t.Errorf("Coord.String() = %v, want %v", got, tt.want)
			}
		})
	}
}
