package worlds

import (
	"image"
	"math/rand"

	"gitlab.com/tploss/biosim/sim"
)

type square struct {
	size     uint8
	min, max uint8
	grid     [][]gridpoint // TODO: could be turned into a separate struct that is based on a single slice (since 'width' is known)
}

type gridpoint struct {
	sim.Coord
	free bool
}

func NewSquareWorld(size uint8) sim.World {
	grid := make([][]gridpoint, size)
	var x, y uint8
	for x = 0; x < size; x++ {
		grid[x] = make([]gridpoint, size)
		for y = 0; y < size; y++ {
			c := sim.Coord{X: x, Y: y}
			gp := gridpoint{c, true}
			grid[x][y] = gp
		}
	}
	return &square{size, 0, size - 1, grid}
}

func (w *square) Clone() sim.World {
	gridCopy := make([][]gridpoint, len(w.grid))
	for i := range w.grid {
		gridCopy[i] = make([]gridpoint, len(w.grid[i]))
		copy(gridCopy[i], w.grid[i])
	}

	return &square{w.size, w.min, w.max, gridCopy}
}

func (w *square) Settle() sim.Coord {
	c := w.rand()
	w.Place(c)
	return c
}

// if run on a non existant location this is a no-op
func (w *square) Place(c sim.Coord) bool {
	if !w.Free(c) {
		return false
	}
	w.grid[c.X][c.Y].free = false
	return true
}

// new position is set; if Move not possible sim.Coord will be unchanged
func (w *square) Move(from *sim.Coord, direction uint8) {
	if w.grid[from.X][from.Y].free {
		return
	}
	to := w.next(*from, direction)
	if !w.grid[to.X][to.Y].free {
		return
	}
	// keep track of free/blocked gridpoints
	w.grid[from.X][from.Y].free = true
	w.grid[to.X][to.Y].free = false
	from.X, from.Y = to.X, to.Y
}

// out of bounds Coord will be reported as not free
func (w *square) Free(c sim.Coord) bool {
	if w.invalidCoord(c) {
		return false
	}
	return w.grid[c.X][c.Y].free
}

// if the function that is run on every gridpoint returns true walking is ended
func (w *square) Walk(do sim.WalkFunc) bool {
	var x, y uint8
	for x = 0; x < w.size; x++ {
		for y = 0; y < w.size; y++ {
			if do(w.grid[x][y].Coord) {
				return true
			}
		}
	}
	return false
}

func (w *square) Rect() image.Rectangle {
	min, max := int(w.min), int(w.max)+1
	return image.Rect(min, min, max, max)
}

// will protect against under-/ overflow by returning same sim.Coord
// if an invalid direction is given same sim.Coord is returned
func (w *square) next(from sim.Coord, direction uint8) sim.Coord {
	if (from.Y == w.min && (direction == sim.North || direction == sim.NorthEast || direction == sim.NorthWest)) ||
		(from.Y == w.max && (direction == sim.South || direction == sim.SouthEast || direction == sim.SouthWest)) ||
		(from.X == w.max && (direction == sim.East || direction == sim.NorthEast || direction == sim.SouthEast)) ||
		(from.X == w.min && (direction == sim.West || direction == sim.NorthWest || direction == sim.SouthWest)) {
		return from
	}
	switch direction {
	case sim.North:
		return sim.Coord{X: from.X, Y: from.Y - 1}
	case sim.NorthEast:
		return sim.Coord{X: from.X + 1, Y: from.Y - 1}
	case sim.NorthWest:
		return sim.Coord{X: from.X - 1, Y: from.Y - 1}
	case sim.South:
		return sim.Coord{X: from.X, Y: from.Y + 1}
	case sim.SouthEast:
		return sim.Coord{X: from.X + 1, Y: from.Y + 1}
	case sim.SouthWest:
		return sim.Coord{X: from.X - 1, Y: from.Y + 1}
	case sim.East:
		return sim.Coord{X: from.X + 1, Y: from.Y}
	case sim.West:
		return sim.Coord{X: from.X - 1, Y: from.Y}
	default:
		return from
	}
}

func (w *square) invalidCoord(c sim.Coord) bool {
	return c.X > w.max || c.Y > w.max || c.X < w.min || c.Y < w.min
}

func (w *square) rand() sim.Coord {
	var x, y uint8
	for {
		x, y = uint8(rand.Intn(int(w.max)+1)), uint8(rand.Intn(int(w.max)+1))
		c := sim.Coord{X: x, Y: y}
		if w.Free(c) {
			return c
		}
		for _, dir := range sim.Directions {
			if next := w.next(c, dir); w.Free(next) {
				return next
			}
		}
	}
}
