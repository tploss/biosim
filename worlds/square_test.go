package worlds

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/tploss/biosim/sim"
)

func Test_square_Clone(t *testing.T) {
	c0 := sim.Coord{X: 0, Y: 0}
	c1 := sim.Coord{X: 1, Y: 1}
	c2 := sim.Coord{X: 2, Y: 2}
	w1 := NewSquareWorld(3)
	w1.Place(c0)
	w2 := w1.Clone()
	w1.Place(c1)
	w2.Place(c2)
	if !w1.Free(c2) {
		t.Errorf("%v should have only be placed on world 2", c2)
	}
	if !w2.Free(c1) {
		t.Errorf("%v should have only be placed on world 1", c1)
	}
}

func Test_square_Move(t *testing.T) {
	type test struct {
		size      uint8
		place     bool
		from      sim.Coord
		direction uint8
		want      sim.Coord
	}
	everyDirectionTests := []test{
		{
			3,
			true,
			sim.Coord{X: 1, Y: 1},
			sim.North,
			sim.Coord{X: 1, Y: 0},
		},
		{
			3,
			true,
			sim.Coord{X: 1, Y: 1},
			sim.NorthEast,
			sim.Coord{X: 2, Y: 0},
		},
		{
			3,
			true,
			sim.Coord{X: 1, Y: 1},
			sim.East,
			sim.Coord{X: 2, Y: 1},
		},
		{
			3,
			true,
			sim.Coord{X: 1, Y: 1},
			sim.SouthEast,
			sim.Coord{X: 2, Y: 2},
		},
		{
			3,
			true,
			sim.Coord{X: 1, Y: 1},
			sim.South,
			sim.Coord{X: 1, Y: 2},
		},
		{
			3,
			true,
			sim.Coord{X: 1, Y: 1},
			sim.SouthWest,
			sim.Coord{X: 0, Y: 2},
		},
		{
			3,
			true,
			sim.Coord{X: 1, Y: 1},
			sim.West,
			sim.Coord{X: 0, Y: 1},
		},
		{
			3,
			true,
			sim.Coord{X: 1, Y: 1},
			sim.NorthWest,
			sim.Coord{X: 0, Y: 0},
		},
	}
	moveOutOfBoundsTests := []test{
		{
			1,
			true,
			sim.Coord{X: 0, Y: 0},
			sim.North,
			sim.Coord{X: 0, Y: 0},
		},
		{
			1,
			true,
			sim.Coord{X: 0, Y: 0},
			sim.East,
			sim.Coord{X: 0, Y: 0},
		},
		{
			1,
			true,
			sim.Coord{X: 0, Y: 0},
			sim.South,
			sim.Coord{X: 0, Y: 0},
		},
		{
			1,
			true,
			sim.Coord{X: 0, Y: 0},
			sim.West,
			sim.Coord{X: 0, Y: 0},
		},
	}
	errorCaseTests := []test{
		{
			3,
			false,
			sim.Coord{X: 0, Y: 0},
			sim.North,
			sim.Coord{X: 0, Y: 0},
		},
		{
			3,
			true,
			sim.Coord{X: 0, Y: 0},
			8, // invalid direction
			sim.Coord{X: 0, Y: 0},
		},
	}

	tests := make([]test, 0)
	tests = append(tests, everyDirectionTests...)
	tests = append(tests, moveOutOfBoundsTests...)
	tests = append(tests, errorCaseTests...)
	for _, tt := range tests {
		name := fmt.Sprintf("world size %d, place %t, from %v, direction %d",
			tt.size, tt.place, tt.from, tt.direction)
		t.Run(name, func(t *testing.T) {
			w := NewSquareWorld(tt.size)
			if tt.place {
				if !w.Place(tt.from) {
					t.Errorf("square.Place(%v) = false, want true; could not place", tt.from)
				}
			}
			if w.Move(&tt.from, tt.direction); !reflect.DeepEqual(tt.from, tt.want) {
				t.Errorf("square.Move() = %v, want %v", tt.from, tt.want)
			}
		})
	}
}

func Test_square_Place(t *testing.T) {
	type test struct {
		sim.Coord
		want bool
	}
	tests := []test{
		// Order is important
		{
			sim.Coord{X: 0, Y: 0},
			true,
		},
		{
			sim.Coord{X: 9, Y: 9},
			true,
		},
		{
			sim.Coord{X: 10, Y: 10},
			false,
		},
		{
			sim.Coord{X: 0, Y: 0},
			false,
		},
	}
	// Place all tests in same world
	w := NewSquareWorld(10)
	for i, tt := range tests {
		name := fmt.Sprintf("%d: %v, expect %t", i, tt.Coord, tt.want)
		t.Run(name, func(t *testing.T) {
			if got := w.Place(tt.Coord); got != tt.want {
				t.Errorf("square.Place() = %v, want %v", got, tt.want)
			}
		})
	}
}
