package vizs

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"os"
	"sync"

	"gitlab.com/tploss/biosim/sim"
)

var free, block, creature color.Color = color.White, color.White, color.RGBA{G: 255, A: 255}

type PngCreator struct {
	Fname   string
	wg      sync.WaitGroup
	fnumber int
}

func (p *PngCreator) Process(w sim.World, pop sim.Population) {
	p.wg.Add(1)
	n := p.fnumber
	p.fnumber++
	go func(w sim.World, pop sim.Population) {
		defer p.wg.Done()
		p.write(n, p.compute(w, pop))
	}(w, pop)
}

func (p *PngCreator) Wait() {
	p.wg.Wait()
}

func (p *PngCreator) compute(w sim.World, pop sim.Population) image.Image {
	img := image.NewRGBA(w.Rect())
	wf := func(c sim.Coord) bool {
		x, y := int(c.X), int(c.Y)
		if w.Free(c) {
			img.Set(x, y, free)
		} else if _, exists := pop[c]; exists {
			img.Set(x, y, creature)
		} else {
			img.Set(x, y, block)
		}
		return false
	}
	w.Walk(wf)
	return img
}

func (p *PngCreator) write(num int, img image.Image) error {
	f, err := p.file(num)
	if err != nil {
		return err
	}
	defer f.Close()
	return png.Encode(f, img)
}

func (p *PngCreator) file(num int) (io.WriteCloser, error) {
	f := fmt.Sprintf("%s_%04d.png", p.Fname, num)
	_, err := os.Stat(f)
	if err != nil {
		return os.Create(f)
	} else {
		return os.Open(f)
	}
}
