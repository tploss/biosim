#!/bin/bash
trap "kill 0" EXIT

for pic in out/*.png; do
	(ffmpeg -loglevel fatal -y -i "${pic}" -frames:v scale="-1:ih*4" "${pic%.*}_s.png" && printf ".")&
done

wait

printf "\nAll scaled\n"

for pic in out/*_s.png; do
	rm -f "${pic%_s*}.png" 
done
