package main

import (
	"flag"
	"log"
	"math"
	"math/rand"

	"gitlab.com/tploss/biosim/creatures"
	"gitlab.com/tploss/biosim/vizs"
	"gitlab.com/tploss/biosim/worlds"
)

var size = flag.Int("size", 254, "set world size to `value`")
var amount = flag.Int("amount", 200, "set amount of creatures to `value`")
var rounds = flag.Int("rounds", 300, "set round amount to `value`")

func run() {
	checkArgs()
	// get a world
	world := worlds.NewSquareWorld(uint8(*size))

	// get a population
	r := func(n int) int {
		rand.Seed(int64(n))
		return rand.Int()
	}
	pop := creatures.Populate(world, *amount, r)

	// act x times
	pc := vizs.PngCreator{Fname: "out/square_dumb_gen_1_round"}
	for i := 0; i < *rounds; i++ {
		pop.Act(world)

		// compute image
		pc.Process(world.Clone(), pop.Clone())
	}
	pc.Wait()
}

func checkArgs() {
	if *size%2 == 1 {
		log.Fatalln("Size has to be an even number")
	}
	if *size > math.MaxUint8 {
		log.Fatalf("Size cannot be bigger than %d\n", math.MaxUint8)
	}
	ss := (*size) * (*size)
	max := ss - ss/3
	if *amount > max {
		log.Fatalf("Amount cannot be bigger than %d so that at least one third of the map is free\n", max)
	}
}
