//go:build !profiling

package main

import (
	"flag"
)

func main() {
	flag.Parse()
	run()
}
