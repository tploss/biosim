package creatures

import "gitlab.com/tploss/biosim/sim"

type Dumb struct {
	sim.Coord
	actionInput int
}

type RandFunc func(int) int

func Populate(w sim.World, n int, r RandFunc) sim.Population {
	pop := make(map[sim.Coord]sim.Creature)
	for i := 0; i < n; i++ {
		c := w.Settle()
		pop[c] = &Dumb{c, r(i)}
	}
	return pop
}

func (c *Dumb) Clone() sim.Creature {
	return &Dumb{c.Coord, c.actionInput}
}

func (c *Dumb) Act(w sim.World) sim.Coord {
	direction := uint8(c.actionInput % 8)
	w.Move(&c.Coord, direction)
	return c.Coord
}
